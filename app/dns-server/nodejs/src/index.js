// noinspection NpmUsedModulesInstalled
const request = require('request');
const dns2 = require('dns2');
const createServer = dns2.createServer;
const Packet = dns2.Packet;
const k8s = require('@kubernetes/client-node');
const _ = require('lodash');

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const opts = {};
kc.applyToRequest(opts);

const dnsPort = parseInt(process.env.DNS_PORT, 10);
const fallbackDNS = new dns2({ dns: '1.1.1.1' });

// See https://tools.ietf.org/html/rfc1034#section-4.3.3
const wildcardRegex = new RegExp('^[*][.](?<anydomain>[^*]+)$');

const respond = (dnsRequest, dnsResponseSend) => {

    const names = [];
    for (let i = 0; i < dnsRequest.questions.length; i++) {
        const name = dnsRequest.questions[i].name;
        names.push(name);
    }

    request.get(`${kc.getCurrentCluster().server}/apis/networking.k8s.io/v1/ingresses`, opts, async (error, response, jsonBody) => {

        const confirmedNames = [];
        const unconfirmedNames = [...names]; // Clone of names for fallback lookup

        const body = JSON.parse(jsonBody);
        for (let i = 0; i < body.items.length; i++) {
            const ingress = body.items[i];
            const rules = ingress.spec.rules;
            for (let k = 0; k < rules.length; k++) {
                const rule = rules[k];
                const host = rule.host;
                if (typeof host === "undefined") {
                    continue;
                }
                const index = unconfirmedNames.indexOf(host);
                if (index !== -1) {
                    confirmedNames.push(host);
                    unconfirmedNames.splice(index, 1); // Remove confirmed name from fallback lookup
                } else {
                    const match = host.match(wildcardRegex);
                    if (match) {
                        const hostRegex = new RegExp(`[^*]+[.]${_.escapeRegExp(match.groups.anydomain)}`);
                        for (const name of names) {
                            if (name.match(hostRegex)) {
                                confirmedNames.push(name);
                                unconfirmedNames.splice(unconfirmedNames.indexOf(name), 1); // Remove confirmed name from fallback lookup
                            }
                        }
                    }
                }
            }
        }

        // If there are unconfirmed names, query them against the fallback DNS
        const fallbackResults = await Promise.all(unconfirmedNames.map(name => fallbackDNS.resolveA(name)));

        const dnsResponse = Packet.createResponseFromRequest(dnsRequest);
        dnsResponse.header.qr = 1;
        dnsResponse.header.ra = 1;
        dnsResponse.additionals = [];

        for (let i = 0; i < confirmedNames.length; i++) {
            dnsResponse.answers.push({
                address: process.env.INGRESS_IP || process.env.POD_IP,
                type: Packet.TYPE.A,
                class: Packet.CLASS.IN,
                ttl: 300,
                name: confirmedNames[i]
            });
        }

        // Add fallback results to the response
        fallbackResults.forEach(result => {
            if (result.answers.length) {
                result.answers.forEach(answer => {
                    dnsResponse.answers.push(answer);
                });
            }
        });


        dnsResponseSend(dnsResponse);
    });
};


const server = dns2.createServer({
    udp: true,
    handle: (request, send, rinfo) => {
        respond(request, send)
    }
});

server.on('request', (request, response, rinfo) => {
    console.log(request.header.id, request.questions[0]);
});

server.on('requestError', (error) => {
    console.log('Client sent an invalid request', error);
});

server.on('listening', () => {
    console.log(`Listening to ${process.env.POD_IP} on port ${dnsPort}`);
    console.log(server.addresses());
});

server.on('close', () => {
    console.log('server closed');
});

server.listen({
    // Optionally specify port, address and/or the family of socket() for udp server:
    udp: {
        port: dnsPort,
        address: process.env.POD_IP,
        type: "udp4",  // IPv4 or IPv6 (Must be either "udp4" or "udp6")
    },

    // Optionally specify port and/or address for tcp server:
    tcp: {
        port: dnsPort,
        address: process.env.POD_IP,
    },
});

