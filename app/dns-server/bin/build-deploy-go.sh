#!/bin/sh -e

# Only run this build if files in the dns-server directory have changed.
if [[ ! -z "$(git log -1 --name-only --oneline | grep "app/dns-server/docker\|app/dns-server/go" )" ]]; then
  version=$(jq -r '.version' app/dns-server/go/package.json)
  docker build app/dns-server -f app/dns-server/docker/go/Dockerfile --tag $CI_REGISTRY_IMAGE:$version --tag $CI_REGISTRY_IMAGE:go
  echo "$GIT_SSH_PK" | base64 -d > id_rsa
  chmod 400 id_rsa
  echo 'ssh -i ./id_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $*' > ssh
  chmod +x ssh
  git config --global user.name "$GIT_USER_NAME"
  git config --global user.email "$GIT_USER_EMAIL"
  git tag $version
  git remote set-url origin git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git
  GIT_SSH='./ssh' git push origin $version
      # Docker push goes after git push so that if a tag already exists the push will be rejected to
      # prevent accidentally overwriting versioned tags. If you really want to overwrite a tag
      # first delete it in gitlab
  docker push $CI_REGISTRY_IMAGE:$version
  docker push $CI_REGISTRY_IMAGE:go
fi